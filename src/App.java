import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        conectarBd();
    }

    public static void conectarBd() {
        try {
            // Crear objeto de conexion a la BD
            Connection conn = DriverManager.getConnection("jdbc:sqlite:hr.db");
            if (conn != null) {
                System.out.println("Conexion exitosa");
                Statement st = conn.createStatement();
                // querySelecEmployeetAll(st);
                insertJob(conn);
                selectAllJobs(st);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void querySelecEmployeetAll(Statement st) throws SQLException {
        // Ejecutar consulta SQL
        ResultSet result = st.executeQuery("SELECT * FROM employees");
        String info = "";
        // Iterar los resultados
        while (result.next()) {
            // Capturar nombre
            String nombre = result.getString("first_name");
            String apellido = result.getString("last_name");
            int id = result.getInt("employee_id");
            info += "Id : " + id;
            info += "\nNombre: " + nombre;
            info += "\nApellido: " + apellido;
            info += "\n----------------\n";
        }
        // Imprimir nombre
        System.out.println(info);
    }

    public static void insertJob(Connection conn) throws SQLException {
        // PREPARAR una consulta sql
        String query = "INSERT INTO jobs(job_title, min_salary, max_salary) VALUES(?, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(query);
        // Setear/anexar los valores a la consulta
        pst.setString(1, "Developer");
        pst.setInt(2, 2000);
        pst.setInt(3, 15000);

        // Ejecutar consulta
        pst.executeUpdate();
    }

    public static void selectAllJobs(Statement st) throws SQLException {
        ResultSet result = st.executeQuery("SELECT * FROM jobs");
        while (result.next()) {
            System.out.println("Job: " + result.getString("job_title"));
        }
    }

}
